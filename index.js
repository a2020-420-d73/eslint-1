var x = 5+2;
var z = [1,4,6, 9
];
var o;
var age = 19;
o ={ nom: 'Louis', age: age}

function f1() {
  x=2;
  var x;
  x ++;
  y=x;
  return y;
}


function f2() {
  var x, y;
  x = 3
  for (y=0;y<z.length;y++)
  {
    x=x+z[y];
  }
  return x;
}

function f3(a,b,c) {
  return a*b;
}

function f4() {
  var longObject = { a:1, b:2,c:3,d:4,e:2,f:2,g:2,h:2,i:2,j:2,k:2,l:2,m:2,n:2,o:2,p:2,q:2,r:2,s:2,t:2,u:2,v:2,w:2}
    , sum=0;
  Object.values(longObject).forEach(function(val){sum=sum+val});
  return sum;
}
/* utilisez ces appels pour vérifier que votre code n'est pas dénaturé */
console.log(`${x} === 7`);
console.log(`${f1()} === 3`);
console.log(`${f2()} === 23`);
console.log(`${f3(2, 3)} === 6`);
console.log(`${f3(4, 5)} === 20`);
console.log(`${f4()} === 48`);
